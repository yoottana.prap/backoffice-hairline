# base image
FROM node:18.16.0-slim

# set working directory
WORKDIR /usr/src/app

# add `/usr/src/app/node_modules/.bin` to $PATH
# ENV PATH /usr/src/app/node_modules/.bin:$PATH
ARG PORT
ARG REACT_APP_BASE_URL
ENV PORT 3004
ENV REACT_APP_BASE_URL ${REACT_APP_BASE_URL}
# install and cache app dependencies
COPY package.json /usr/src/app/package.json
RUN npm config rm https-proxy && \
    npm config rm proxy
RUN npm config ls -l
RUN npm config set fetch-retry-mintimeout 20000 && \  
    npm config set fetch-retry-maxtimeout 120000
RUN npm install -g npm@9.6.6
RUN npm install --legacy-peer-deps
RUN npm install react-scripts -g
RUN npm install -g serve

COPY . /usr/src/app/

RUN npm run build


# start app
CMD ["serve", "build", "-s"]