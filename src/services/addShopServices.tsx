import axiosInstance from "./base";


export const createShopService = async (raw: any) => {
    try {

        let data = new FormData();
        console.log(raw.banner[0]);
        
        data.append('photo', raw.banner[0]);
        data.append('address', raw.address);
        data.append('email', raw.email);
        data.append('open', raw.open);
        data.append('close', raw.close);
        data.append('telephone', raw.tel);
        data.append('name', raw.name);
        data.append('lat', raw.lat);
        data.append('lng', raw.lng);
        const resposne = await axiosInstance({
            url: '/shop/shop-management',
            method: 'post',
            data: data
        })
            .then((response) => {
                return response.data
            })
            .catch((error) => {
                return false
            })

        return resposne
    } catch (error) {

    }
}
