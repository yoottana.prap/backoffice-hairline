import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_API_BASE_URL ?? "http:45.91.132.227/apis",
});

// Add a request interceptor
axiosInstance.interceptors.request.use(
    (config) => {
        // Get the token from localStorage or any other source
        const token = localStorage.getItem('access-token');

        // Set the token in the request headers
        if (token) {
            config.headers['Authorization'] = `Bearer ${JSON.parse(token).token}`;
        }

        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

export default axiosInstance;
