import { useEffect, useRef, useState } from "react";
import { IoIosMenu } from "react-icons/io"
import { useNavigate } from "react-router-dom";
import SidebarAdmin from "../components/sidebarAdmin";
export default function MainLayoutAdmin({ children }: any) {
    const navigator = useNavigate()
    useEffect(() => {
        if (!localStorage.getItem('access-token')) {
            navigator('/')
        }
    }, [])

    return (
        <div className="container-fluid">
            <button className="btn bg-white border-none"
                style={{ position: 'fixed', zIndex: '9999', left: -5, top: -5 }}
                data-widget="pushmenu"
            >
                <IoIosMenu size={30} color='gray' />
            </button>
            <div className="row">
                <div className="wrapper pl-0">
                    <SidebarAdmin />
                </div>
                <div className="col-12 col-md-10 col-lg-10  ml-auto mt-5">
                    {children}
                </div>
            </div>
        </div>
    )
}