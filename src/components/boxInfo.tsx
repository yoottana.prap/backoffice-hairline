import { Link } from "react-router-dom";

export default function BoxInfo({ text, amount, color, icon, link }: any) {
    return (
        <div>
            <div className={`small-box bg-${color ?? "info"}`}>
                <div className="inner">
                    <h3>{amount}</h3>
                    <p>{text}</p>
                </div>
                <div className="icon">
                    {icon}
                </div>


                {/* <a href="#" className="small-box-footer">
                </a> */}
                {
                    link ?
                    <Link to={link} className="small-box-footer">
                        More info <i className="fas fa-arrow-circle-right" />
                    </Link>
                    : <></>
                }
            </div>

        </div>
    )
}