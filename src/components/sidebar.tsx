import React from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';

const Sidebar = () => {
  const location = useLocation();
  const navigate = useNavigate()
  const logout = () => {
    localStorage.clear()
    navigate('/')
  }

  return (
    <>
      <aside className="main-sidebar sidebar-dark-primary elevation-4 pt-5">
        <div className="sidebar">
          <div className="user-panel mt-3 pb-3 mb-3 d-flex">
            <div className="info pt-5">
              <Link to="/" className="d-block">
                User Name
              </Link>
            </div>
          </div>

          <nav className="mt-2">
            <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <li className="nav-item">
                <Link to="/admin" className="nav-link">
                  <i className="nav-icon fas fa-tachometer-alt"></i>
                  <p>Dashboard</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/users" className="nav-link">
                  <i className="nav-icon fas fa-users"></i>
                  <p>Users</p>
                </Link>
              </li>
              {/* Add more sidebar items here */}
            </ul>
          </nav>
        </div>
        <div className="sidebar">
          <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li className="nav-item">
              <a
                onClick={logout}
                className={`nav-link text-center`}
                href="#">ออกจากระบบ</a>
            </li>
          </ul>
        </div>
      </aside>
    </>
  );
};

export default Sidebar;
