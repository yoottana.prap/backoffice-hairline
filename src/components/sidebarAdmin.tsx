import React from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';

const Sidebar = () => {
  const location = useLocation();
  const navigate = useNavigate()
  const logout = () => {
    localStorage.clear()
    navigate('/')
  }
  return (
    <aside className="main-sidebar vh-100  sidebar-dark-primary elevation-4 align-items-between">
      <div className="sidebar">
        <div className="user-panel mt-3 pb-3  d-flex">
          <div className="info pt-5">
            <Link to="/admin" className="d-block">
              สวัสดี Admins
            </Link>
          </div>
        </div>

        <nav className="mt-2">
          <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li className="nav-item">
              <Link to="/admin" className={`nav-link ${location.pathname === '/admin' ? 'active' : ''}`}>
                <i className="nav-icon fas fa-tachometer-alt"></i>
                <p>แดชบอร์ด</p>
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/shop-management" className={`nav-link ${location.pathname === '/shop-management' ? 'active' : ''}`}>
                <i className="nav-icon fas fa-users"></i>
                <p>ร้านค้า</p>
              </Link>
            </li>
          </ul>
        </nav>
      </div>
      <div className="sidebar">
        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li className="nav-item">
            <a
              onClick={logout}
              className={`nav-link text-center`}
              href="#">ออกจากระบบ</a>
          </li>
        </ul>
      </div>
    </aside>
  );
};

export default Sidebar;
