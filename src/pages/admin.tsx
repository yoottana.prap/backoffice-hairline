import BoxInfo from "../components/boxInfo"
import MainLayoutAdmin from "../layouts/MainLayoutAdmin"
import { AiFillShop } from "react-icons/ai"
import { LiaUserSolid } from "react-icons/lia"
// import {
//     Chart
// } from 'react-chartjs-2';

import { Chart as ChartJSRegister, BarController, BarElement, CategoryScale, LineElement, PointElement, LinearScale, Title } from 'chart.js';
import DataTable from "react-data-table-component";
import { useEffect, useState } from "react";
import axiosInstance from "../services/base";
import { Link } from "react-router-dom";

ChartJSRegister.register(LineElement, PointElement, CategoryScale, LinearScale, Title, BarController, BarElement);


const Admin = () => {
    const [data, setdata] = useState<any>([])
    const [groupData, setGroupData] = useState<any>([])
    useEffect(() => {
        fetchShop()
    }, [setdata])

    async function fetchShop() {
        try {
            const resp = await axiosInstance.get("auth/dashboard")
            setdata(resp.data.data)
            setGroupData([...resp.data.data.users, ...resp.data.data.shops])
        } catch (error) {

        }
    }
    // const labels = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม']
    // const options = {
    //     maintainAspectRatio: true,
    //     scales: {
    //         y: {
    //             beginAtZero: false,
    //         },
    //     },
    // };
    console.log(data);


    const columns = [
        {
            name: 'ชื่อผู้ใช้งาน',
            selector: (row: any) => `${row.first_name} ${row.last_name}`,
            sortable: true,
        },
        {
            name: 'ประเภทผู้ใช้งาน',
            selector: (row: any) => `${row.member_type}`,
            sortable: true,
        },
        {
            name: 'อีเมล์',
            selector: (row: any) => `${row.email}`,
            sortable: true,
        },
    ]


    return (
        <MainLayoutAdmin>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12 col-lg-4">
                        <BoxInfo text="ร้านค้าทั้งหมด" amount={data?.count?.shop_in_system} color="warning" link="/shop-management" icon={<AiFillShop />} />
                    </div>
                    <div className="col-12 col-lg-4">
                        <BoxInfo text="ผู้ใช้งานทั้งหมด" amount={data?.count?.users} color="success" link="#" icon={<LiaUserSolid />} />
                    </div>

                    <div className="col-12 col-lg-4">
                        <BoxInfo text="ผู้จัดการร้านค้า" amount={data?.count?.shop} color="primary" link="#" icon={<LiaUserSolid />} />
                    </div>
                </div>
                <div className="col-12 d-flex justify-content-between my-5">
                    <h3>ผู้ใช้งาน</h3>
                </div>
                <DataTable
                    columns={columns}
                    data={groupData}
                />
            </div>
        </MainLayoutAdmin>
    )
}

export default Admin