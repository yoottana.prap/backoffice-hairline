import { useEffect, useState } from "react"
import axiosInstance from "../services/base"
import { useNavigate } from "react-router-dom";

const Login = () => {
    const navigate = useNavigate();
    
    const [login, editableLogin] = useState({
        email: '',
        password: ''
    })
    
    useEffect(()=>{
        if(localStorage.getItem('access-token')) {
            if(localStorage.getItem('member_type') === "admin") {
                navigate(`/admin`)
            }else {
                navigate(`/shop`)
            }
        }
    },[])

    const handleSubmit = async () => {
        try {
            const response = await axiosInstance.post("/auth/login", {
                email: login.email,
                password: login.password
            })

            localStorage.setItem('access-token', JSON.stringify(response.data.token))
            localStorage.setItem('member_type', response.data.member_type)
            if (response.data.member_type === "admin") {
                navigate('/admin')
            }
        } catch (error) {

        }
    }

    return (
        <div className="container-fluid bg-light">
            <div className="row">

                <div className="d-md-block  d-none   col-5 vh-100 d-flex justify-content-center align-items-center"
                    style={{
                        backgroundImage: 'url(https://img.freepik.com/premium-vector/secure-login-sign-up-concept-illustration-user-use-secure-login-password-protection-website-social-media-account-vector-flat-style_7737-2270.jpg?w=1480)',
                        backgroundSize: 'cover',
                        backgroundRepeat: 'no-repeat',
                        backgroundPosition: 'center',
                        content: '',
                    }}
                >
                </div>
                <div className="col vh-100 flex-column d-flex justify-content-center align-items-center">
                    <div className="login-box">
                        <div className="card w-100" style={{ minHeight: 400 }}>
                            <div className="card-body login-card-body">
                                <h2 className="login-box-msg">เข้าสู่ระบบ</h2>
                                <div className="input-group mb-3">
                                    <input type="email" className="form-control" placeholder="Email" onChange={(e: any) => {
                                        editableLogin({
                                            ...login,
                                            email: e.target.value
                                        })
                                    }} />
                                </div>
                                <div className="input-group mb-3">
                                    <input type="password" className="form-control" placeholder="Password" onChange={(e: any) => {
                                        editableLogin({
                                            ...login,
                                            password: e.target.value
                                        })
                                    }} />
                                </div>
                                <div className="row">
                                    <div className="col-4 ml-auto">
                                        <button className="btn btn-primary btn-block" onClick={() => handleSubmit()}>Sign In</button>
                                    </div>
                                </div>
                                <div className="social-auth-links text-center mt-5">
                                    <p className="mb-1">
                                        <a href="forgot-password.html">I forgot my password</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    )
}

export default Login