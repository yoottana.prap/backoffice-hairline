import { useForm } from "react-hook-form";
import MainLayoutAdmin from "../layouts/MainLayoutAdmin"
import { createShopService } from "../services/addShopServices";
import { useNavigate } from "react-router-dom";

const AddShop = () => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();

    const navigate = useNavigate()

    const submitForm = async (data: any) => {
        try {
            const response = await createShopService(data)
            if (response) {
                alert('สร้างร้านค้าสำเร็จ')
                navigate("/shop-management")
            } else {
                alert('สร้างร้านค้าล้มเหลว')
            }
        } catch (error) {
            alert('ระบบมีปัญหากรุณาติดต่อทีมพัฒนา')
        }
    }
    return (
        <MainLayoutAdmin>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12 d-flex justify-content-between mb-2">
                        <h3>เพิ่มร้านค้า</h3>
                    </div>
                    <div className="col-12">
                        <form onSubmit={handleSubmit(submitForm)}>
                            <div className="form-group">
                                <label htmlFor="name">ชื่อร้าน</label>
                                <input
                                    {...register("name", { required: true })}
                                    type="text" className="form-control" id="name" placeholder="ชื่อร้าน" />
                            </div>
                            <div className="container-fluid px-0 mb-3">
                                <div className="row">
                                    <div className="col-6">
                                        <label>เวลาเปิด</label>
                                        <input
                                            {...register("open", { required: true })}
                                            type="time" className="form-control" id="open" placeholder="ละติจูด" />
                                    </div>

                                    <div className="col-6">
                                        <label >เวลาปิด</label>
                                        <input
                                            {...register("close", { required: true })}
                                            type="time" className="form-control" id="close" placeholder="ลองจิจูด" />
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">อีเมล์สำหรับเข้าสู่ระบบ</label>
                                <input
                                    {...register("email", { required: true })}
                                    type="text" className="form-control" id="email" placeholder="อีเมล์สำหรับเข้าสู่ระบบ" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="tel">หมายเลขโทรศัพท์</label>
                                <input
                                    {...register("tel", { required: true })}
                                    type="text" className="form-control" id="tel" placeholder="หมายเลขโทรศัพท์" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="address">ที่อยู่</label>
                                <textarea
                                    {...register("address", { required: true })}
                                    className="form-control" id="address" rows={3} defaultValue={""} />
                            </div>
                            <div className="form-group">
                                <a href="https://www.google.co.th/maps" target="_blank" rel="noreferrer">หาที่ตั้งในแผนที่</a> <br />
                                <label >ตำแหน่งที่ตั้ง</label>
                                <div className="container-fluid px-0">
                                    <div className="row">
                                        <div className="col-6">
                                            <input
                                                {...register("lat", { required: true })}
                                                type="text" className="form-control" id="lat" placeholder="ละติจูด" />
                                        </div>

                                        <div className="col-6">
                                            <input
                                                {...register("lng", { required: true })}
                                                type="text" className="form-control" id="lng" placeholder="ลองจิจูด" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="form-group border py-5 px-2">
                                <label htmlFor="banner">แบนเนอร์ร้านค้า</label>
                                <input
                                    {...register("banner")}
                                    type="file" className="form-control-file" id="banner" />
                            </div>

                            <div className="d-flex w-100 justify-content-end">
                                <button className="btn btn-primary ml-auto my-5" type="submit">บันทึกข้อมูล</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div >
        </MainLayoutAdmin >
    )
}

export default AddShop