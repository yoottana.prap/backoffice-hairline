import DataTable from "react-data-table-component"
import MainLayoutAdmin from "../layouts/MainLayoutAdmin"
import { useEffect, useState } from "react";
import axios from "axios";
import moment from "moment";
import { Link } from "react-router-dom";
import axiosInstance from "../services/base";

const ShopManagement = () => {

    const [data, setData] = useState([])
    const columns = [
        {
            name: 'รูปร้าน',
            selector: (row: any) => row.cover,
            sortable: false,
            cell: (row: any) => {
                return (
                    <img className="my-2 w-75" src={row.cover} alt={row.name} />
                )
            }
        },
        {
            name: 'ชื่อร้าน',
            selector: (row: any) => row.name,
            sortable: true,
        },
        {
            name: 'เบอร์โทร',
            selector: (row: any) => row.telephone
        },
        {
            name: 'เวลาเปิด - ปิด',
            selector: (row: any) => row.open,
            cell: (row: any) => {
                return (
                    <div className="d-flex">
                        <div className="mt-2">
                            {moment(row.open, 'HH:mm').format("HH:mm")}น. - {moment(row.close, 'HH:mm').format("HH:mm")}น.
                        </div>
                    </div>
                )
            }
        },
        {
            name: 'ที่อยู่',
            sortable: true,
            width: '30%',
            cell: (row: any) => {
                return (
                    <div className="d-flex">
                        <div className="mt-2">
                            <p>
                                {row.lat} {row.lng}
                            </p>
                            <p>
                                {row.address}
                            </p>
                        </div>
                    </div>
                )
            }
        },
        {
            name: 'การจัดการ',
            sortable: false,
            cell: (row: any) => {
                return (
                    <>
                        <button onClick={() => alert('test')} className="btn btn-danger">ระงับ</button>
                    </>
                )
            }
        },
    ];

    useEffect(() => {
        fetchStore()
    }, [])

    const fetchStore = async () => {
        const storeLists = await axiosInstance.get('/shop/lists').then((res) => {
            return res.data.data
        })
        setData(storeLists)
    }
    const testing: any = (e: any) => {
        console.log(e)
    }
    return (
        <MainLayoutAdmin>

            <div className="col-12 d-flex justify-content-between mb-2">
                <h3>ร้านค้าในระบบ</h3>
                <div>
                    <Link className="btn btn-primary text-white text-decoration-none" to={'/add-shop'}>เพิ่มร้านค้า</Link>
                </div>
            </div>
            <div className="col-12">
                <DataTable
                    selectableRows
                    onSelectedRowsChange={testing}
                    columns={columns}
                    data={data}
                />
            </div>
        </MainLayoutAdmin>
    )
}

export default ShopManagement