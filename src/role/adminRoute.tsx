import { useEffect } from "react"
import { useNavigate } from "react-router-dom"

const AdminRoute = ({ children }: any) => {
    const navigate = useNavigate()

    useEffect(() => {
        if (localStorage.getItem('member_type') !== "admin") {
            navigate('/')
        }
    })
    return (
        <>
            {children}
        </>
    )
}

export default AdminRoute