import { useEffect } from "react"
import { useNavigate } from "react-router-dom"

const ShopRoute = ({ children }: any) => {
    const navigate = useNavigate()

    useEffect(() => {
        if (localStorage.getItem('member_type') !== "shop") {
            navigate('/')
        }
    })
    return (
        <>
            {children}
        </>
    )
}

export default ShopRoute