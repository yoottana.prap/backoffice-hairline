import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import Login from "./pages/index"
import "../node_modules/admin-lte/dist/js/adminlte.min.js"
import "../node_modules/admin-lte/dist/css/adminlte.min.css"
import "../node_modules/jquery/dist/jquery"
import "../node_modules/bootstrap/dist/css/bootstrap.min.css"
import "../node_modules/bootstrap/dist/js/bootstrap.min.js"
import "../node_modules/@popperjs/core/dist/umd/popper.min.js"

import Admin from './pages/admin';
import ShopRoute from './role/shopRoute';
import AdminRoute from './role/adminRoute';
import Shop from './pages/shop';
import ShopManagement from './pages/shopManagement';
import AddShop from './pages/addShop';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Login />} />

        <Route path="/admin" element={
          <AdminRoute>
            <Admin />
          </AdminRoute>
        } />
        <Route path="/shop-management" element={
          <AdminRoute>
            <ShopManagement />
          </AdminRoute>
        } />
        <Route path="/add-shop" element={
          <AdminRoute>
            <AddShop />
          </AdminRoute>
        } />

        <Route path="/shop" element={
          <ShopRoute>
            <Shop />
          </ShopRoute>
        } />

      </Routes>
    </Router>
  );
}

export default App;
